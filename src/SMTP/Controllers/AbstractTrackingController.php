<?php

namespace CloudZentral\SMTP\Controllers;

use Illuminate\Http\Request;

/**
 * Class AbstractTrackingController
 * @package CloudZentral\SMTP\Controllers
 */
abstract class AbstractTrackingController extends Controller
{
    /**
     * Log tracking.
     * @param Request $request
     */
    public function logTracking(Request $request)
    {
        $gif = public_path("cloudzentral/laravel-smtp/images/tracking.gif");
        header('Content-Type: image/gif');
        readfile($gif);

        $this->update($request);

        exit;
    }

    /**
     * Update tracking.
     * @param Request $request
     */
    abstract protected function update(Request $request): void;
}
