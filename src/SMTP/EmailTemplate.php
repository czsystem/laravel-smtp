<?php

namespace CloudZentral\SMTP;

use CloudZentral\SMTP\EmailTemplates\DefaultEmailTemplate;
use CloudZentral\Templates\Template;
use CloudZentral\Templates\Traits\Widgetable;

/**
 * Class EmailTemplate
 * @package CloudZentral\SMTP
 */
abstract class EmailTemplate extends Template
{
    use Widgetable;

    /**
     * TYPES.
     */
    const TYPE_DEFAULT = "1";

    /**
     * @inheritDoc
     */
    public static function make(string $type)
    {
        switch($type) {
            case "1": // Default
                return new DefaultEmailTemplate();
            default:
                return null;
        }
    }
}
