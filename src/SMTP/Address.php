<?php

namespace CloudZentral\SMTP;

/**
 * Class Address
 * @package CloudZentral\SMTP
 */
abstract class Address
{
    /**
     * @var string
     */
    public $name;

    /**
     * @var string
     */
    public $address;

    /**
     * Address constructor.
     * @param string $address
     * @param string|null $name
     */
    public function __construct(string $address, string $name = null)
    {
        $this->name = $name;
        $this->address = $address;
    }
}
