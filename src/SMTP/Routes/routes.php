<?php

use CloudZentral\SMTP\Exceptions\ConfigMissingException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

Route::group([
    'prefix' => 'smtp',
    'as' => 'smtp.'
], function() {
    Route::group([
        'prefix' => 'tracking',
        'as' => 'tracking.'
    ], function() {

        Route::get('log', function(Request $request) {
            $controller = config('laravel-smtp.tracking.controller', false);
            if(!$controller) {
                throw new ConfigMissingException("laravel-smtp.tracking.controller is missing");
            }
            return app($controller)->logTracking($request);
        })->name('log');

    });

});
