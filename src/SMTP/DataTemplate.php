<?php

namespace CloudZentral\SMTP;

use CloudZentral\SMTP\DataTemplates\InvoiceDataTemplate;
use CloudZentral\SMTP\DataTemplates\WUPRDataTemplate;
use CloudZentral\Templates\Template;

/**
 * Class DataTemplate
 * @package CloudZentral\SMTP
 */
abstract class DataTemplate extends Template
{
    /**
     * TYPE CONSTANTS.
     */
    const TYPE_INVOICE = "1";
    const TYPE_WEBSITE_USER_PASSWORD_RESET = "2";

    /**
     * @inheritDoc
     */
    public static function make(string $type)
    {
        switch($type) {
            case "1": // Invoice
                return new InvoiceDataTemplate();
            case "2": // Website user password reset
                return new WUPRDataTemplate();
            default:
                return null;
        }
    }
}
