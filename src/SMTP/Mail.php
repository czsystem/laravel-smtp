<?php

namespace CloudZentral\SMTP;

use Illuminate\Mail\Message;
use Illuminate\Support\Collection;

/**
 * Class Mail
 * @package CloudZentral\SMTP
 */
class Mail
{
    /**
     * Content types.
     */
    const CONTENT_TYPE_TEXT = null;
    const CONTENT_TYPE_HTML = "text/html";

    /**
     * @var string
     */
    private $subject;

    /**
     * @var string
     */
    private $body;

    /**
     * @var Collection
     */
    private $receivers;

    /**
     * @var string
     */
    private $contentType;

    /**
     * @var array
     */
    private $trackingParameters;

    /**
     * @var Collection
     */
    private $dataAttachments;

    /**
     * Mail constructor.
     * @param string $contentType
     */
    public function __construct(string $contentType = self::CONTENT_TYPE_TEXT)
    {
        $this->subject = "";
        $this->body = "";
        $this->receivers = Collection::make([]);
        $this->contentType = $contentType;
        $this->trackingParameters = [];
        $this->dataAttachments = Collection::make([]);
    }

    /**
     * Send mail.
     */
    public function send()
    {
        $subject = $this->subject;
        $body = $this->getBody($this->body);
        $receivers = $this->receivers;
        $contentType = $this->contentType;
        $dataAttachments = $this->dataAttachments;

        \Illuminate\Support\Facades\Mail::send([], [], function(Message $message) use (
            $subject,
            $body,
            $receivers,
            $contentType,
            $dataAttachments
        ) {
            $message->subject($subject)
                ->setBody($body, $contentType);

            foreach ($receivers as $receiver) {
                if($receiver->bcc) {
                    $message->addBcc($receiver->address, $receiver->name);
                } else {
                    $message->addTo($receiver->address, $receiver->name);
                }
            }

            foreach($dataAttachments as $dataAttachment) {
                $message->attachData($dataAttachment->data, $dataAttachment->name);
            }
        });
    }

    /**
     * Get body.
     * @param string $body
     * @return string
     */
    private function getBody(string $body)
    {
        if(!is_null($this->trackingParameters)) {
            $url = route('smtp.tracking.log', $this->trackingParameters);
            if(!is_null(config('laravel-smtp.tracking.url', null))) {
                $params = [];
                foreach($this->trackingParameters as $key => $value) {
                    $params[] = $key."=".$value;
                }
                $url = config('laravel-smtp.tracking.url');
                if(!empty($params)) {
                    $url .= "?".implode("&", $params);
                }
            }
            $body .= '<img src="'.$url.'" alt="">';
        }
        return $body;
    }

    /**
     * Set subject.
     * @param string $subject
     */
    public function setSubject(string $subject)
    {
        $this->subject = $subject;
    }

    /**
     * Set body.
     * @param string $body
     */
    public function setBody(string $body)
    {
        $this->body = $body;
    }

    /**
     * Add receiver.
     * @param Receiver $receiver
     */
    public function addReceiver(Receiver $receiver)
    {
        $this->receivers->add($receiver);
    }

    /**
     * Add receivers.
     * @param Collection $receivers
     */
    public function addReceivers(Collection $receivers)
    {
        foreach($receivers as $receiver) {
            $this->addReceiver($receiver);
        }
    }

    /**
     * Set tracking parameters.
     * @param array $trackingParameters
     */
    public function setTrackingParameters(array $trackingParameters)
    {
        $this->trackingParameters = $trackingParameters;
    }

    /**
     * Add data attachment.
     * @param DataAttachment $dataAttachment
     */
    public function addDataAttachment(DataAttachment $dataAttachment)
    {
        $this->dataAttachments->add($dataAttachment);
    }
}
