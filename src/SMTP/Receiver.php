<?php

namespace CloudZentral\SMTP;

/**
 * Class Receiver
 * @package CloudZentral\SMTP
 */
class Receiver extends Address
{
    /**
     * @var bool
     */
    public $bcc;

    /**
     * @inheritDoc
     * @param bool $bcc
     */
    public function __construct(string $address, string $name = null, bool $bcc = false)
    {
        parent::__construct($address, $name);
        $this->bcc = $bcc;
    }
}
