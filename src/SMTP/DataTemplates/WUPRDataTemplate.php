<?php

namespace CloudZentral\SMTP\DataTemplates;

use CloudZentral\SMTP\DataTemplate;

/**
 * Class WUPRDataTemplate
 * Website user password reset data template
 * @package CloudZentral\SMTP\DataTemplates
 */
class WUPRDataTemplate extends DataTemplate
{
    /**
     * @inheritDoc
     */
    public function getView(array $attributes)
    {
        return view('laravel-smtp::data_templates.wurp', $attributes);
    }

    /**
     * @inheritDoc
     */
    public function getDefaultAttributes(): array
    {
        return [
            'html' => null,
            'link_href' => null,
            'link_text' => null
        ];
    }
}
