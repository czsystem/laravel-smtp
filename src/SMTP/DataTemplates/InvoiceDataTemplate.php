<?php

namespace CloudZentral\SMTP\DataTemplates;

use CloudZentral\SMTP\DataTemplate;

/**
 * Class InvoiceDataTemplate
 * @package CloudZentral\SMTP\DataTemplates
 */
class InvoiceDataTemplate extends DataTemplate
{
    /**
     * @inheritDoc
     */
    public function getView(array $attributes)
    {
        return view('laravel-smtp::data_templates.invoice', $attributes);
    }

    /**
     * @inheritDoc
     * order_lines = Collection<InvoiceOrderLine>
     */
    public function getDefaultAttributes(): array
    {
        return [
            'external_order_id' => null,
            'transaction_id' => null,
            'order_lines' => [],
            'order_line_total' => "",
            'order_total' => "",
            'order_total_before_vat' => "",
            'order_vat' => "",
            'order_vat_percent' => "",
            'order_promo_total' => "",
            'card_fees' => null,
            'remarks' => "",

            'contact_name' => null,
            'contact_address' => null,
            'contact_zip' => null,
            'contact_city' => null,
            'contact_email' => null,
            'contact_phone' => null,
            'contact_mobile_phone' => null,
            'contact_company_number' => null,

            'trans_invoice' => null,
            'trans_customer' => null,
            'trans_customer_number' => null,
            'trans_order' => null,
            'trans_order_id' => null,
            'trans_transaction_id' => null,
            'trans_products' => null,
            'trans_item_number' => null,
            'trans_product' => null,
            'trans_quantity' => null,
            'trans_price' => null,
            'trans_sub_total' => null,
            'trans_total_before_vat' => null,
            'trans_vat' => null,
            'trans_invoice_total' => null,
            'trans_card_fees' => null,
            'trans_remarks' => null,
            'trans_promo' => null,
            'trans_line_total' => null
        ];
    }
}
