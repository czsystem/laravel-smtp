<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>Demystifying Email Design</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
</head>
<body bgcolor="{{ $root_bgcolor }}">
<table bgcolor="{{ $root_bgcolor }}" border="0" cellpadding="0" cellspacing="0" width="100%">
    <tr>
        <td style="padding: 10px 0 30px 0;">
            <table align="center" border="0" cellpadding="0" cellspacing="0" width="{{ $root_width }}">
                <!-- TOP -->
                @if( $top_column1_content || $top_column2_content || $top_column3_content )
                    <tr>
                        <td bgcolor="{{ $top_bgcolor }}" style="padding: 40px 0 30px 0;">
                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <!-- COLUMNS -->
                                    @if($top_column1_content)
                                        <td
                                            align="center"
                                            style="color: {{ $top_color }}; font-size: 28px; font-weight: bold; font-family: Arial, sans-serif;"
                                        >{!! $top_column1_content !!}</td>
                                    @endif
                                    @if($top_column2_content)
                                        <td
                                            align="center"
                                            style="color: {{ $top_color }}; font-size: 28px; font-weight: bold; font-family: Arial, sans-serif;"
                                        >{!! $top_column2_content !!}</td>
                                    @endif
                                    @if($top_column3_content)
                                        <td
                                            align="center"
                                            style="color: {{ $top_color }}; font-size: 28px; font-weight: bold; font-family: Arial, sans-serif;"
                                        >{!! $top_column3_content !!}</td>
                                    @endif
                                </tr>
                            </table>
                        </td>
                    </tr>
                @endif

            <!-- BODY -->
                <tr>
                    <td bgcolor="{{ $body_bgcolor }}" style="color: {{ $body_color }}; padding: 40px 30px 40px 30px;">
                        {!! $body_content !!}
                    </td>
                </tr>

                <!-- BOTTOM -->
                @if( $bottom_column1_content || $bottom_column2_content || $bottom_column3_content )
                    <tr>
                        <td bgcolor="{{ $bottom_bgcolor }}" style="padding: 30px 30px 40px 30px;">
                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <!-- COLUMNS -->
                                    @if($bottom_column1_content)
                                        <td
                                            align="center"
                                            style="color: {{ $bottom_color }}; font-size: 14px; font-family: Arial, sans-serif;"
                                        >{!! $bottom_column1_content !!}</td>
                                    @endif
                                    @if($bottom_column2_content)
                                        <td
                                            align="center"
                                            style="color: {{ $bottom_color }}; font-size: 14px; font-family: Arial, sans-serif;"
                                        >{!! $bottom_column2_content !!}</td>
                                    @endif
                                    @if($bottom_column3_content)
                                        <td
                                            align="center"
                                            style="color: {{ $bottom_color }}; font-size: 14px; font-family: Arial, sans-serif;"
                                        >{!! $bottom_column3_content !!}</td>
                                    @endif
                                </tr>
                            </table>
                        </td>
                    </tr>
                @endif
            </table>
        </td>
    </tr>
</table>
</body>
</html>
