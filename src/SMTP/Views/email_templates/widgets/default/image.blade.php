@php
    $max_width = $imagewidth;
    list($originalWidth, $originalHeight) = getimagesize($src);
    $ratio = $originalWidth / $originalHeight;
    $height = (int)round($max_width / $ratio);
@endphp

<img src="{{ $src }}" width="{{ $max_width }}" height="{{ $height }}" alt="{{ $alt }}" style="display: block;">
