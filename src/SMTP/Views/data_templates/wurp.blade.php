<table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
    @if($html)
        <tr>
            <td>{!! $html !!}</td>
        </tr>
    @endif
    @if($link_href && $link_text)
        <tr>
            <td><a href="{{ $link_href }}">{{ $link_text }}</a></td>
        </tr>
    @endif
</table>
