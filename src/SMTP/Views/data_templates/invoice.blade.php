<table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
    <tr>
        <td colspan="2"><h1>{{ $trans_invoice }}</h1></td>
    </tr>
    <tr>
        <!-- CONTACT INFO -->
        <td valign="top">
            <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                <tr>
                    <td><h2>{{ $trans_customer }}</h2></td>
                </tr>
                <tr>
                    <td>{{ $contact_name }}</td>
                </tr>
                <tr>
                    <td>{{ $contact_address }}</td>
                </tr>
                <tr>
                    <td>{{ $contact_zip }} {{ $contact_city }}</td>
                </tr>
                @if(!is_null($contact_company_number))
                    <tr>
                        <td>{{ $trans_customer_number }}: {{ $contact_company_number }}</td>
                    </tr>
                @endif
            </table>
        </td>

        <!-- ORDER INFO -->
        <td valign="top" align="right">
            <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                <tr>
                    <td align="right"><h2>{{ $trans_order }}</h2></td>
                </tr>
                <tr>
                    <td align="right">{{ $trans_order_id }}: {{ $external_order_id }}</td>
                </tr>
                <tr>
                    <td align="right">{{ $trans_transaction_id }}: {{ $transaction_id }}</td>
                </tr>
                <tr>
                    <td align="right">{{ $trans_remarks }}: {{ $remarks }}</td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td valign="top"><h2 style="margin-top: 32.370px">{{ $trans_products }}</h2></td>
    </tr>
    <tr>
        <!-- ORDER LINES -->
        <td valign="top" colspan="2">
            <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                <tr>
                    <td style="border-bottom: 1px solid black;">{{ $trans_item_number }}</td>
                    <td style="border-bottom: 1px solid black;">{{ $trans_product }}</td>
                    <td align="right" style="border-bottom: 1px solid black;">{{ $trans_quantity }}</td>
                    <td align="right" style="border-bottom: 1px solid black;">{{ $trans_price }}</td>
                    <td align="right" style="border-bottom: 1px solid black;">{{ $trans_sub_total }}</td>
                </tr>
                @foreach($order_lines as $order_line)
                    <tr>
                        <td>{{ $order_line->item_number }}</td>
                        <td>{{ $order_line->text }}</td>
                        <td align="right">{{ $order_line->amount }}</td>
                        <td align="right">{{ $order_line->price }}</td>
                        <td align="right">{{ $order_line->subTotal }}</td>
                    </tr>
                @endforeach

                <!-- LINE TOTAL -->
                <tr>
                    <td align="right" colspan="4" style="border-top: 1px solid black;">
                        {{ $trans_line_total }}
                    </td>
                    <td align="right" style="border-top: 1px solid black;">
                        {{ $order_line_total }}
                    </td>
                </tr>

                @if($order_promo_total !== "")
                    <!-- PROMO TOTAL -->
                    <tr>
                        <td align="right" colspan="4">
                            {{ $trans_promo }}
                        </td>
                        <td align="right">
                            {{ $order_promo_total }}
                        </td>
                    </tr>
                @endif

                <!-- TOTAL BEFORE VAT -->
                <tr>
                    <td align="right" colspan="4">
                        {{ $trans_total_before_vat }}
                    </td>
                    <td align="right">
                        {{ $order_total_before_vat }}
                    </td>
                </tr>

                <!-- VAT-->
                <tr>
                    <td align="right" colspan="4">
                        {{ $trans_vat }} {{ $order_vat_percent }}%
                    </td>
                    <td align="right">
                        {{ $order_vat }}
                    </td>
                </tr>

                <!-- FEE -->
                @if(!is_null($card_fees) && $card_fees !== 0)
                    <tr>
                        <td align="right" colspan="4" style="border-top: 1px solid black;">
                            {{ $trans_card_fees }}
                        </td>
                        <td align="right" style="border-top: 1px solid black;">
                            {{ $card_fees }}
                        </td>
                    </tr>
                @endif

                <!-- TOTAL -->
                <tr>
                    <td align="right" colspan="4">
                        {{ $trans_invoice_total }}
                    </td>
                    <td align="right">
                        {{ $order_total }}
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
