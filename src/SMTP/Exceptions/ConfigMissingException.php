<?php

namespace CloudZentral\SMTP\Exceptions;

/**
 * Class ConfigMissingException
 * @package CloudZentral\SMTP\Exceptions
 */
class ConfigMissingException extends \Exception
{
    //
}
