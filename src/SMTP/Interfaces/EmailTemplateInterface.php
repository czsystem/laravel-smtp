<?php

namespace CloudZentral\SMTP\Interfaces;

use Illuminate\Contracts\View\Factory;
use Illuminate\View\View;
use Throwable;

/**
 * Interface EmailTemplateInterface
 * @package CloudZentral\SMTP\Interfaces
 */
interface EmailTemplateInterface
{
    /**
     * Render the e-mail template view.
     * @param array $attributes
     * @return string
     * @throws Throwable
     */
    public function render(array $attributes = []): string;

    /**
     * Get the the e-mail template view.
     * @param array $attributes
     * @return Factory|View
     */
    public function getView(array $attributes);

    /**
     * Get default attributes.
     * @return array
     */
    public function getDefaultAttributes(): array;
}
