<?php

namespace CloudZentral\SMTP\Interfaces;

use Illuminate\Contracts\View\Factory;
use Illuminate\View\View;
use Throwable;

/**
 * Interface DataTemplateInterface
 * @package CloudZentral\SMTP\Interfaces
 */
interface DataTemplateInterface
{
    /**
     * Render the data template view.
     * @param array $attributes
     * @return string
     * @throws Throwable
     */
    public function render(array $attributes = []): string;

    /**
     * Get the the data template view.
     * @param array $attributes
     * @return Factory|View
     */
    public function getView(array $attributes);

    /**
     * Get default attributes.
     * @return array
     */
    public function getDefaultAttributes(): array;
}
