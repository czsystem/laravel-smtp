<?php

namespace CloudZentral\SMTP\Interfaces;

use Illuminate\View\View;
use Throwable;

/**
 * Interface WidgetDefinerInterface
 * @package CloudZentral\SMTP\Interfaces
 */
interface WidgetDefinerInterface
{
    /**
     * Render a widget
     * @param string|null $type
     * @param mixed ...$attributes
     * @return String|null
     * @throws Throwable
     */
    public function renderWidget(?string $type, ...$attributes): ?String;

    /**
     * Render text widget view.
     * @param string|null $text
     * @return string|null
     * @throws Throwable
     */
    public function renderTextWidget(?string $text): ?string;

    /**
     * Render image widget view.
     * @param string|null $alt
     * @param string|null $src
     * @return string|null
     * @throws Throwable
     */
    public function renderImageWidget(?string $alt, ?string $src, ?string $imagewidth): ?string;

    /**
     * Get text widget view.
     * @param string|null $text
     * @return View
     */
    public function getTextWidgetView(?string $text): View;

    /**
     * Get image widget view.
     * @param string|null $alt
     * @param string|null $src
     * @return View
     */
    public function getImageWidgetView(?string $alt, ?string $src, ?string $imagewidth): View;
}
