<?php

namespace CloudZentral\SMTP\Providers;

/**
 * Class ServiceProvider
 * @package CloudZentral\SMTP\Providers
 */
class ServiceProvider extends \Illuminate\Support\ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadViewsFrom(__DIR__.'/../Views', 'laravel-smtp');
        $this->loadRoutesFrom(__DIR__.'/../Routes/routes.php');
        $this->publishes([
            __DIR__.'/../Views' => resource_path('views/cloudzentral/laravel-smtp')
        ]);

        $this->publishes([
            __DIR__.'/../Assets' => public_path('cloudzentral/laravel-smtp')
        ], ['public', 'tracking']);
        $this->publishes([
            __DIR__.'/../Configs/laravel-smtp.php' => config_path('laravel-smtp.php')
        ], ['config', 'tracking']);
    }
}
