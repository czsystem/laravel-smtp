<?php

namespace CloudZentral\SMTP;

use Illuminate\Support\Collection;

/**
 * Class Client
 * @package CloudZentral\SMTP
 */
class Client
{
    /**
     * Encryption types.
     */
    const ENCRYPTION_NONE   = "";
    const ENCRYPTION_TLS    = "tls";
    const ENCRYPTION_SSL    = "ssl";

    /**
     * Content types.
     */
    const CONTENT_TYPE_TEXT = null;
    const CONTENT_TYPE_HTML = "text/html";

    /**
     * @var Client
     */
    private static $instance;

    /**
     * @var string
     */
    private $host;

    /**
     * @var string
     */
    private $port;

    /**
     * @var string
     */
    private $username;

    /**
     * @var string
     */
    private $password;

    /**
     * @var string
     */
    private $encryption;

    /**
     * @var Sender
     */
    private $from;

    /**
     * Get client instance.
     * @param string|null $host
     * @param string|null $port
     * @param string|null $username
     * @param string|null $password
     * @param Sender|null $from
     * @param string|null $encryption
     * @param bool $reset
     * @return Client
     */
    public static function getInstance(
        ?string $host,
        ?string $port,
        ?string $username,
        ?string $password,
        ?Sender $from,
        ?string $encryption = self::ENCRYPTION_NONE,
        bool $reset = false
    )
    {
        if(is_null(self::$instance) || $reset) {
            return self::$instance = new self(
                $host,
                $port,
                $username,
                $password,
                $from,
                $encryption
            );
        } else {
            return self::$instance;
        }
    }

    /**
     * Client constructor.
     * @param string $host
     * @param string $port
     * @param string $username
     * @param string $password
     * @param Sender $from
     * @param string $encryption
     */
    private function __construct(
        string $host,
        string $port,
        string $username,
        string $password,
        Sender $from,
        string $encryption
    )
    {
        $this->host = $host;
        $this->port = $port;
        $this->username = $username;
        $this->password = $password;
        $this->from = $from;
        $this->encryption = $encryption;
        $this->overwriteConfig();
    }

    /**
     * Overwrite laravel build-in mail config.
     */
    private function overwriteConfig()
    {
        $existing = config('mail');
        $new = array_merge(
            $existing,
            [
                'host'          => $this->host,
                'port'          => $this->port,
                'username'      => $this->username,
                'password'      => $this->password,
                'from'          => [
                    'address' => $this->from->address,
                    'name' => $this->from->name
                ],
                'encryption'    => $this->encryption
            ]
        );
        config(['mail' => $new]);
    }

    /**
     * Send a mail.
     * @param string $subject
     * @param string $body
     * @param Collection $receivers
     * @param string $contentType
     * @param array|null $trackingParams
     */
    public function sendEmail(string $subject, string $body, Collection $receivers, string $contentType = Mail::CONTENT_TYPE_TEXT, ?array $trackingParams = null)
    {
        $mail = new Mail($contentType);
        $mail->setSubject($subject);
        $mail->setBody($body);
        $mail->addReceivers($receivers);
        if(!is_null($trackingParams)) {
            $mail->setTrackingParameters($trackingParams);
        }
        $mail->send();
    }

    /**
     * Get mail instance.
     * @param string|null $contentType
     * @return Mail
     */
    public function getMail(string $contentType = Mail::CONTENT_TYPE_TEXT)
    {
        return new Mail($contentType);
    }
}
