<?php

namespace CloudZentral\SMTP;

/**
 * Class DataAttachment
 * @package CloudZentral\SMTP
 */
class DataAttachment
{
    /**
     * @var string
     */
    public $name;

    /**
     * @var string
     */
    public $data;

    /**
     * AttachedData constructor.
     * @param string $name
     * @param string $data
     */
    public function __construct(string $name, string $data)
    {
        $this->name = $name;
        $this->data = $data;
    }
}
