<?php

namespace CloudZentral\SMTP\EmailTemplates;

use CloudZentral\SMTP\EmailTemplate;
use Illuminate\View\View;

/**
 * Class DefaultEmailTemplate
 * @package CloudZentral\SMTP
 */
class DefaultEmailTemplate extends EmailTemplate
{
    /**
     * @inheritDoc
     */
    public function getDefaultAttributes(): array
    {
        return [
            'root_bgcolor' => "#ffffff",
            'root_width' => "800",

            'top_bgcolor' => "#ffffff",
            'top_color' => "#000000",

            'top_column1_content' => null,
            'top_column2_content' => null,
            'top_column3_content' => null,

            'body_bgcolor' => "#ffffff",
            'body_color' => "#000000",
            'body_content' => null,

            'bottom_bgcolor' => "#ffffff",
            'bottom_color' => "#000000",

            'bottom_column1_content' => null,
            'bottom_column2_content' => null,
            'bottom_column3_content' => null,
        ];
    }

    /**
     * @inheritDoc
     */
    public function getView(array $attributes = [])
    {
        return view('laravel-smtp::email_templates.default')
            ->with($attributes);
    }

    /**
     * @inheritDoc
     */
    public function getTextWidgetView(?string $text): View
    {
        return view("laravel-smtp::email_templates.widgets.default.text", [
            'text' => $text
        ]);
    }

    /**
     * @inheritDoc
     */
    public function getImageWidgetView(?string $alt, ?string $src, ?string $imagewidth): View
    {
        return view("laravel-smtp::email_templates.widgets.default.image", [
            'src' => $src,
            'alt' => $alt,
            'imagewidth' => $imagewidth
        ]);
    }
}
